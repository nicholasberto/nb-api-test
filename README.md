# PHP Developer - Backend Test

### About
This software is owned by hotel group that needs to be able to expose its data to external booking services.


### Requirements

- PHP 7.3.3+
- composer
- mySql database

### Installing the project

```bash
git clone https://gitlab.com/nicholasberto/nb-api-test.git nb-api-test
cd nb-api-test
cp .env.dist .env
composer install
```

Then you need to fill *DATABASE_URL* parameter in the .env file with th url of your database.

Finally you have to import file **example.sql** in tour database.

### Running the project
The software is composed by 3 API routes, two for get rooms list and room availabilities and one for place the book.
##### GET /get-structures?
example
```bash
/get-structures?filter[city]=Milano
``` 
This route returns the list of structures owned by the group and takes as parameter in the query string:
- filter (optional - allows you to filter values, valid only on city)

Example response:
```bash
{
    "success": true,
    "code": 200,
    "results": [
        {
            "id": 2,
            "name": "Hotel Duomo",
            "city": "Milano"
        }
    ]
}
```

##### GET /get-structure-availabilities/{id}
example
```bash
/get-structure-availabilities/1?startDate=2022-09-29&endDate=2022-10-03
``` 
This route returns the list of availabilities of the requested hotel and takes as parameters:
- startDate (YYYY-MM-DD)
- endDate (YYYY-MM-DD)

Example response:
```bash
{
    "success": true,
    "code": 200,
    "results": [
        {
            "id": "3",
            "name": "Standard Room",
            "beds": "2",
            "size": "22mq",
            "nights": "4",
            "costPerNight": "40.00 EUR",
            "totalCost": "160.00 EUR"
        },
        {
            "id": "1",
            "name": "Standard Room",
            "beds": "2",
            "size": "22mq",
            "nights": "4",
            "costPerNight": "40.00 EUR",
            "totalCost": "160.00 EUR"
        },
        {
            "id": "13",
            "name": "Junior Suite",
            "beds": "2",
            "size": "40mq",
            "nights": "4",
            "costPerNight": "100.00 EUR",
            "totalCost": "400.00 EUR"
        }
}
```

##### POST /book
example
```bash
POST /book
Host: foo.com
Content-Type: application/json

{
  "room": "3", 
  "startDate": "2022-11-02", 
  "endDate": "2022-11-07", 
  "email": "jon@foo.com"
}
``` 
This route checks the room availability and places the book:
- room (the room ID)
- startDate (YYYY-MM-DD)
- endDate (YYYY-MM-DD)
- email (email of the user)