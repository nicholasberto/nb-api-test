<?php

namespace App\Component\Response;

use Symfony\Component\HttpFoundation\JsonResponse;

class PrettyJsonResponse extends JsonResponse
{
    protected $encodingOptions = self::DEFAULT_ENCODING_OPTIONS | JSON_PRETTY_PRINT;
}