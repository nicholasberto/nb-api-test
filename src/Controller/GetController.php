<?php

namespace App\Controller;

use App\Component\Response\PrettyJsonResponse;
use App\Entity\Hotel;
use App\Entity\Room;
use App\Repository\HotelRepository;
use App\Repository\RoomRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class GetController extends AbstractController
{
    /**
     * @Route("/get-structures", name="get_user_monthly_amount")
     * @Method("GET")
     */
    public function getStructures(Request $request)
    {
        if(is_array($request->get('filter'))) {
            $diff = array_diff(array_flip($request->get('filter')), $this->getParameter('get_structures_available_filters'));

            if(!empty($diff)) {
                return new PrettyJsonResponse([
                    'success' => false,
                    'code' => 400,
                    'error' => [
                        'message' => 'Bad Request',
                        'debug' => 'Not allowed filters: ' . implode(', ', array_keys($diff))
                    ]
                ], 400);
            }
        }

        $em = $this->getDoctrine()->getManager();

        /** @var HotelRepository $repoHotel */
        $repoHotel = $em->getRepository(Hotel::class);
        $results = $repoHotel->getStructures(is_array($request->get('filter')) ? $request->get('filter') : []);

        if(empty($results)) {
            return new PrettyJsonResponse([
                'success' => false,
                'code' => 404,
                'error' => [
                    'message' => 'Not found',
                    'debug' => 'Not found'
                ]
            ], 404);
        }

        return new PrettyJsonResponse([
            'success' => true,
            'code' => 200,
            'results' => $results
        ]);
    }

    /**
     * @Route("/get-structure-availabilities/{id}", name="get_structure_availabilites")
     * @Method("GET")
     */
    public function getStructureAvailabilities(Request $request, $id)
    {
        $checkFields = preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $request->get('startDate')) && preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $request->get('endDate'));

        if(!$checkFields) {
            return new PrettyJsonResponse([
                'success' => false,
                'code' => 400,
                'error' => [
                    'message' => 'Bad Request',
                    'debug' => 'Some mandatory fields are missing or not formatted correctly.'
                ]
            ], 400);
        }

        $startDate = \DateTime::createFromFormat('Y-m-d H:i:s', $request->get('startDate') . '14:00:00');
        $endDate = \DateTime::createFromFormat('Y-m-d H:i:s', $request->get('endDate') . '10:00:00');

        if($startDate >= $endDate) {
            return new PrettyJsonResponse([
                'success' => false,
                'code' => 400,
                'error' => [
                    'message' => 'Bad Request',
                    'debug' => 'endDate must be greater than startDate.'
                ]
            ], 400);
        }

        $em = $this->getDoctrine()->getManager();

        $hotel = $em->getRepository(Hotel::class)->find($id);

        if(!$hotel instanceof Hotel) {
            return new PrettyJsonResponse([
                'success' => false,
                'code' => 404,
                'error' => [
                    'message' => 'Non found',
                    'debug' => 'Hotel not found.'
                ]
            ], 404);
        }

        /** @var RoomRepository $repoRooms */
        $repoRooms = $em->getRepository(Room::class);
        $results = $repoRooms->findAvailabilities($hotel->getId(), $startDate, $endDate);

        return new PrettyJsonResponse([
            'success' => true,
            'code' => 200,
            'results' => $results
        ]);
    }

}
