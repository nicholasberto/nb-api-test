<?php

namespace App\Controller;

use App\Component\Response\PrettyJsonResponse;
use App\Entity\Booking;
use App\Entity\Room;
use App\Repository\RoomRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class PostController extends AbstractController
{
    /**
     * @Route("/book", name="book")
     * @Method("POST")
     */
    public function bookAction(Request $request)
    {
        $bookRequest = json_decode($request->getContent(), true);

        $missing = array_diff($this->getParameter('book_post_fields'), array_flip($bookRequest));
        $notAllowed = array_diff(array_flip($bookRequest), $this->getParameter('book_post_fields'));

        if(!empty($missing)) {
            return new PrettyJsonResponse([
                'success' => false,
                'code' => 400,
                'error' => [
                    'message' => 'Bad Request',
                    'debug' => 'Some mandatory fields are missing.'
                ]
            ], 400);
        }

        if(!empty($notAllowed)) {
            return new PrettyJsonResponse([
                'success' => false,
                'code' => 400,
                'error' => [
                    'message' => 'Bad Request',
                    'debug' => 'Not allowed fields.'
                ]
            ], 400);
        }

        $checkFields = preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $bookRequest['startDate']) && preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $bookRequest['endDate']);

        if(!$checkFields) {
            return new PrettyJsonResponse([
                'success' => false,
                'code' => 400,
                'error' => [
                    'message' => 'Bad Request',
                    'debug' => 'Book dates not formatted correctly.'
                ]
            ], 400);
        }

        $startDate = \DateTime::createFromFormat('Y-m-d H:i:s', $bookRequest['startDate'] . '14:00:00');
        $endDate = \DateTime::createFromFormat('Y-m-d H:i:s', $bookRequest['endDate'] . '10:00:00');

        if($startDate >= $endDate) {
            return new PrettyJsonResponse([
                'success' => false,
                'code' => 400,
                'error' => [
                    'message' => 'Bad Request',
                    'debug' => 'endDate must be greater than startDate.'
                ]
            ], 400);
        }

        $em = $this->getDoctrine()->getManager();

        $room = $em->getRepository(Room::class)->find($bookRequest['room']);

        if(!$room instanceof Room) {
            return new PrettyJsonResponse([
                'success' => false,
                'code' => 404,
                'error' => [
                    'message' => 'Non found',
                    'debug' => 'Room not found.'
                ]
            ], 404);
        }

        /** @var RoomRepository $repoRooms */
        $repoRooms = $em->getRepository(Room::class);
        $availability = $repoRooms->findAvailabilities($room->getHotel()->getId(), $startDate, $endDate, $room->getId());

        if(empty($availability)) {
            return new PrettyJsonResponse([
                'success' => false,
                'code' => 404,
                'error' => [
                    'message' => 'Non found',
                    'debug' => 'Zero availabilities found for this room.'
                ]
            ], 404);
        }

        $booking = new Booking();
        $booking->setCheckinDate($startDate);
        $booking->setCheckoutDate($endDate);
        $booking->setHotel($room->getHotel());
        $booking->setRoom($room);
        $booking->setEmail($bookRequest['email']);
        $booking->setTotalCost($availability[0]['totalCost']);

        $em->persist($booking);
        $em->flush();

        return new PrettyJsonResponse([
            'success' => true,
            'code' => 200
        ]);
    }
}
