<?php

namespace App\Repository;

use App\Entity\Booking;
use App\Entity\Room;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Room>
 *
 * @method Room|null find($id, $lockMode = null, $lockVersion = null)
 * @method Room|null findOneBy(array $criteria, array $orderBy = null)
 * @method Room[]    findAll()
 * @method Room[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RoomRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Room::class);
    }

    public function add(Room $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Room $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function findAvailabilities($hotel, $startDate, $endDate, $room = false)
    {
        $em = $this->getEntityManager();
        $where = $room ? ' AND r.id = ' . $room : '';

        $sql = "
            SELECT 
                r.id AS 'id',
                r.name AS 'name',
                r.beds AS 'beds',
                r.size AS 'size',
                DATEDIFF(:endDate, :startDate) AS 'nights',
                CONCAT(r.cost_per_night, ' EUR') AS 'costPerNight',
                CONCAT(DATEDIFF(:endDate, :startDate) * r.cost_per_night, ' EUR') AS 'totalCost'
            FROM 
                room r
            LEFT JOIN 
                booking b ON b.room_id = r.id
            WHERE
                r.hotel_id = :hotel AND (:startDate > b.checkout_date OR :endDate < b.checkin_date OR b.room_id IS NULL) ".$where."
            ;"
        ;

        $stmt = $em->getConnection()->prepare($sql);
        $resultSet = $stmt->executeQuery([
            'startDate' => $startDate->format('Y-m-d H:i:s'),
            'endDate' => $endDate->format('Y-m-d H:i:s'),
            'hotel' => $hotel
        ]);

        return $resultSet->fetchAllAssociative();
    }
}
